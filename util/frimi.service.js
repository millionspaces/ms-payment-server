const config = require('config');
const axios = require('axios');
const qs = require('qs');
const moment = require('moment');

function getFriMiToken() {
    return axios({
        method: 'post',
        url: config.get('frimi-config.token_path'),
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': config.get('frimi-config.token_auth')
        },
        data: qs.stringify({ grant_type: 'client_credentials' })
    })
}

function submitFrimiRequest(accessToken, requestBody) {
    // console.log('accessToken', accessToken);
    return axios({
        method: 'post',
        url: config.get('frimi-config.api_path'),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${accessToken}`
        },
        data: JSON.stringify(requestBody)
    })
}

function generateRequestId() {
    return config.get('frimi-config.tid') + moment(new Date()).format("MMDDSS");
}

module.exports = {
    getFriMiToken,
    submitFrimiRequest,
    generateRequestId
}

