const config = require('config');
const cryptoService = require('./crypto.service');
const NUMBER_OF_KEYS = 12;
const moment = require('moment');

module.exports = {
    getFormattedAmount,
    generateRequestId,
    getUPaySignature,
    testEncrypt
}

function getUPaySignature(referenceId, totalAmount) {
    const url = `${config.get('upay_config.service_code')}|${referenceId}|${totalAmount}|${config.get('upay_config.currency')}|${config.get('upay_config.return_url')}|${config.get('upay_config.notify_url')}|${config.get('upay_config.cancel_url')}`;
    return cryptoService.hmacSHA256(url, config.get('upay_config.signature'));
}

function getFormattedAmount(amount) {
    if(amount)
        return pad(amount, NUMBER_OF_KEYS);
}

function pad (value, numberOfKeys) {
    value = value.toString();
    return value.length < numberOfKeys ? pad("0" + value, numberOfKeys) : value;
}

function generateRequestId() {
    return config.get('frimi-config.tid') + pad(moment(new Date()).format("DD"), 6);
}

// NOTE: test function for AES encryption and decryption
function testEncrypt() {
    const string = '{"service_code":"R0011-MS1-MPS1","currency":"LKR","return_url":"https://qae.millionspaces.com","cancel_url":"https://qae.millionspaces.com","notify_url":"https://qapi.millionspaces.com/api/payment/upay/response","signature":"TWlsbGlvbiBTcGFjZXM=","end_point":"https://sandbox.upay.lk/v1/make-payment"}';
    const password = 'abcd1234abcd1234';

    const encrypted = cryptoService.AESEncrypt(string, password);
    console.log('encrypted >>> ', encrypted);

    const decrypted = cryptoService.AESDecrypt(encrypted, password);
    console.log('decrypted >>> ', decrypted);
}


