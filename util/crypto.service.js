const CryptoJS = require("crypto-js");
const encrypt_options = { mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Iso97971 };

module.exports = {
    
    sha1hex: function(message) {
        return toHex(CryptoJS.SHA1, message);
    },
    packHex: function(value) {
        let result = '';
        const source = value.length % 2 ? value + '0' : value;

        for(let i = 0; i < source.length; i = i + 2) {
            result += String.fromCharCode(parseInt( source.substr( i , 2 ), 16));
        }
        return result;
    },

    
    hmacSHA256: function(text, secretKey) {
        return CryptoJS.HmacSHA256(text, secretKey).toString();
    },
    AESEncrypt: function(string, password) {
        return CryptoJS.AES.encrypt(string, password, encrypt_options).toString();
    },
    AESDecrypt: function(encrypted, password) {
        const decrypted = CryptoJS.AES.decrypt(encrypted, password, encrypt_options);
        return decrypted.toString(CryptoJS.enc.Utf8);
    }
}

function toHex(hasher, message){
    return hasher(message).toString(CryptoJS.enc.Hex);
}