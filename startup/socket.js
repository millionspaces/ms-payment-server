const express = require('express');
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const session = require('express-session');
const cookieParser = require('cookie-parser');

module.exports = function (server) {

    let io = require('socket.io').listen(server);

    io.sockets.on('connection', function (socket) {
        
        console.log("connected - %s", socket.id);
        test(socket.id);
    
        // Disconnect
        socket.on('disconnect', function () {
            console.log('disconnected');
        });
    
        socket.on('message', function (data) {
            console.log(data);
            socket.emit('message', data);
        });
    
        function test(id) {
            socket.join(id);
            console.log("id [%s]", JSON.stringify(io.sockets.adapter.rooms[id]));
            console.log("connected clients [%s]", io.sockets.server.eio.clientsCount);
            console.log("clients: [%s]", JSON.stringify(io.sockets.adapter.rooms));
        }
    });

};