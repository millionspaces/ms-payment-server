const exphbs = require('express-handlebars');

module.exports = function(app) {
     const hbs = exphbs.create({
         helpers: {
             'if_eq': function(arg1, arg2, options) {
                 return (arg1 == arg2) ? options.fn(this) : options.inverse(this)
             }
         },
         defaultLayout: 'main',
         layoutsDir: __dirname + '/../views/layouts'
     });
    app.set('views', __dirname + '/../views');
    app.engine('handlebars', hbs.engine)
    app.set('view engine', 'handlebars');

}