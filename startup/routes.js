const payments = require('../routes/payments');
const error = require('../middleware/server-error');

module.exports = function (app) {
    app.get('/health', (req, res) => res.send(`OK - ${process.env.NODE_ENV}`));
    app.use('/api/payment', payments);
    app.use(error);
}