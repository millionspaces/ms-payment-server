const express = require('express');
const config = require('config');
const moment = require('moment');
const axios = require('axios');
const paymentUtils = require('../util/payment-util');
const router = express.Router();
const rp = require('request-promise');
const fetch = require('node-fetch');

// NOTE: test route for check AES encryption and decryption
router.get('/cr', (req, res) => {
  paymentUtils.testEncrypt();
  res.send('ok');
});

// path => /api/payment

/**
 *  display main payment option page
 */
router.post('/', (req, res) => {
  console.log('req.body', req.body);
  req.session.requestBody = req.body;
  res.render('payment-summary', {
    bookingInfo: req.body
  });
});

router.post('/onlineTransfer', async (req, res) => {
  try {
    console.log('req.cookie1', req.cookies);
    req.session.requestBody = req.body;

    // let cookie = new tough.Cookie({
    //     key: "EventspaceRest",
    //     value: "",
    //     domain: 'qapi.millionspaces.com',
    //     httpOnly: true,
    //     maxAge: 31536000
    // });

    // // Put cookie in an jar which can be used across multiple requests
    // var cookiejar = rp.jar();
    // cookiejar.setCookie(cookie, 'https://qapi.millionspaces.com');

    /*
         const request_options = {
            uri: config.get('api') + '/api/book/space',
            method: 'PUT',
            body:  { booking_id: 5912, event: 1, method: 'MANUAL', status: 1 },
            json: true,
            headers: { 'User-Agent': 'test' },
         };

        console.log('request_options>>>>>>>>>', request_options);
         const onlineTransferResponse = await processRequest(request_options);
         console.log('onlineTransferResponse', onlineTransferResponse);
    
    */

    fetch(config.get('api') + '/api/book/space', {
      method: 'put',
      body: JSON.stringify({
        booking_id: 5912,
        event: 1,
        method: 'MANUAL',
        status: 1
      }),
      headers: { 'Content-Type': 'application/json' }
    })
      .then(function(response) {
        console.log('success', response);
      })
      .catch(function(error) {
        console.log('error', error);
      });

    // res.render('online-transfer-response', {
    //     bookingInfo: req.body
    // });
  } catch (error) {
    console.error('error', error);
  }
});

// NOTE: test route
router.get('/', (req, res) => {
  res.render('payment-summary', {
    helpers: {
      test: function() {
        console.info('test');
      }
    }
  });
});

/**
 *  display payment response of IPG transaction
 */
router.get('/response', (req, res) => {
  const { status, code } = req.query;
  res.render('ipg-payment-response', {
    input: {
      status,
      code
    }
  });
});

/**
 *  display payment response of paycorp transaction
 */
router.get('/status', (req, res) => {
  const { code, description } = req.query;

  console.log('req.query', req.query);
  res.render('paycorp-payment-response', {
    input: { code, description }
  });
});

/**
 *  continue with one payment option
 */
router.post('/process', async (req, res) => {
  //console.log('req.body', req.body);

  console.log('inside process >>>>>>> ', req.body);
  const paymentOption = +req.body['payment-option'];
  let view = null;

  console.log('paymentOption >>>>>>>>>> ', paymentOption);

  const transferData = {
    bookingId: req.body.bookingId,
    referenceId: req.body.referenceId,
    total: req.body.total,
    apiPath: config.get('api'),
    bank_deposit_details: config.get('sampath_bank_deposit_details')
  };

  try {
    switch (paymentOption) {
      case 1:
        view = 'payment-ipg';
        const signature = paymentUtils.getSignature(
          req.body.referenceId,
          req.body.total.replace('.', '')
        );
        // console.log('SIGNATURE', signature);
        const orderId =
          config.get('ipg_config.sentry_prefix') + req.body.referenceId;
        const purchaseAmt = paymentUtils.getFormattedAmount(
          req.body.total.replace('.', '')
        );
        transferData.ipg = {
          version: config.get('ipg_config.version'),
          merId: config.get('ipg_config.merId'),
          acqId: config.get('ipg_config.acqId'),
          merRespURL: config.get('ipg_config.merRespURL'),
          purchaseCurrency: config.get('ipg_config.purchaseCurrency'),
          purchaseCurrencyExponent: config.get(
            'ipg_config.purchaseCurrencyExponent'
          ),
          signatureMethod: config.get('ipg_config.signatureMethod'),
          captureFlag: config.get('ipg_config.captureFlag'),
          orderId,
          signature,
          purchaseAmt
        };
        break;
      case 35:
        view = 'upay';
        const referenceId = req.body.referenceId;
        const totalAmount = Math.floor(+req.body.total) * 100; // mutiply by 100, UPay will divide total by 100
        const upaySignature = paymentUtils.getUPaySignature(
          referenceId,
          totalAmount
        );
        transferData.upay = {
          serviceCode: config.get('upay_config.service_code'),
          currency: config.get('upay_config.currency'),
          returnUrl: config.get('upay_config.return_url'),
          cancelUrl: config.get('upay_config.cancel_url'),
          notifyUrl: config.get('upay_config.notify_url'),
          endPoint: config.get('upay_config.end_point'),
          signature: upaySignature,
          referenceId,
          totalAmount
        };
        console.log('transferData.upay >>>>>>>>>>>>>>>', transferData.upay);
        break;
      case 2:
        view = 'online-transfer';
        break;
      case 4:
        view = 'payment-bnpl';
        break;
      case 34:
        view = 'payment-paycorp';
        const request_options = {
          uri: config.get('paycorp_config.request_url'),
          method: 'POST',
          body: { id: transferData.bookingId },
          json: true
        };
        console.log(
          'before paycorp >>>>>>>>>>>>>',
          JSON.stringify(request_options)
        );
        // transferData.paycorp_response = await processRequest(request_options);
        transferData.paycorp_response = await processRequest(request_options);
        console.log(
          'after paycorp >>>>>>>>>>>>>',
          JSON.stringify(transferData.paycorp_response)
        );
        break;
      case 7:
        view = 'frimi';
        break;
    }
  } catch (e) {
    console.log('ex', e);
  }

  res.render(view, {
    input: transferData
  });
});

function processRequest(options) {
  return rp(options);
}

/**
 *  process FriMi request
 */
/*router.post('/frimi/action', (req, res) => {

    try {
        const friMiRequest = {
            tid: config.get('frimi-config.tid'),
            request_id: frimiService.generateRequestId(),
            app_id: config.get('frimi-config.app_id'),
            module_id: config.get('frimi-config.module_id'),
            req_type_id: config.get('frimi-config.req_type_id'),
            date_time: moment(new Date()).format("DD-MMM-YYYY HH:mm:ss"),
            sender_id: config.get('frimi-config.sender_id'),
        }

        const friMiBody = {
            frimi_id: req.body.frimi_id,
            merchant_ref_no: req.body.referenceId,
            txn_amount: req.body.total,
            txn_currency_code: config.get('frimi-config.txn_currency_code'),
            mid: config.get('frimi-config.mid'),
            mobile_no: "",
            discount_amount: "0.00",
            description: "",
            custom_field_01: "",
            custom_field_02: ""
        }

        friMiRequest.body = Buffer.from(JSON.stringify(friMiBody)).toString('base64');

        const tokenResponse = await frimiService.getFriMiToken();
        const frimiResponse = await frimiService
            .submitFrimiRequest(tokenResponse.data.access_token, friMiRequest);

        console.log('frimiResponse.data', frimiResponse.data);

        const respBody = JSON.parse(Buffer.from(frimiResponse.data.body, 'base64').toString("utf8"));
        console.log('respBody', respBody);

        res.render('frimi', { input: respBody })

    } catch (error) {
        console.error('error', error);
        throw error;
    }

})*/

module.exports = router;
