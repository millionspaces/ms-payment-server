const express = require('express');
const winston = require('winston');
process.env.NODE_CONFIG_DIR = __dirname + '/config/';
const app = express();

winston.add(winston.transports.File, { json: false, filename: 'server.log' });
winston.handleExceptions(
    new winston.transports.File({ json: false, filename: 'uncaughtExceptions.log' })
);

process.on('unhandledRejection', (ex) => {
    throw ex;
});

require('./startup/config')(app);
require('./startup/middleware')(app);
require('./startup/routes')(app);


const port = process.env.PORT || 3000;

//app.listen(port, () => winston.info(`MS Payment Services listening on port ${port}!`));
const server = require('http').createServer(app);
server.listen(port, function () {
    console.log("ms-notification app is listening at http://%s:%s", 'localhost', server.address().port);
});


require('./startup/socket')(server);
