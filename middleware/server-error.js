const winston = require('winston');

/**
 * handle request processing errors
 * @param err
 * @param req
 * @param res
 * @param next
 */
module.exports = (err, req, res, next) => {
    winston.error(err.message);
    console.log(err);
    res.status(500).send(JSON.stringify(err.message));
}